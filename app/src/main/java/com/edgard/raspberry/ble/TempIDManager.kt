package com.edgard.raspberry.ble

import android.content.Context
import com.edgard.raspberry.ble.logging.CentralLog
import java.io.File
object TempIDManager {

    private const val TAG = "TempIDManager"

    fun storeTemporaryIDs(context: Context, packet: String) {
        CentralLog.d(TAG, "[TempID] Storing temporary IDs into internal storage...")
        val file = File(context.filesDir, "tempIDs")
        file.writeText(packet)
    }

    fun retrieveTemporaryID(): TemporaryID? {

            return TemporaryID(System.currentTimeMillis()*1000, "100", System.currentTimeMillis()*60*1000)
    }


}

package com.edgard.raspberry.ble.streetpass

class BlacklistEntry(val uniqueIdentifier: String, val timeEntered: Long)

package com.edgard.raspberry.ble.streetpass

import com.edgard.raspberry.ble.TemporaryID
import com.edgard.raspberry.ble.logging.CentralLog
import java.io.Console

class StreetPassRecord(
    var v: Int,
    var msg: String,
    var org: String,
    val modelP: String,
    val modelC: String,
    val rssi: Int,
    val txPower: Int?
) {
    companion object {
        private const val TAG = "Record"
    }
    init {
        CentralLog.d(
            TAG,
            "[record]   ${v}, ${msg}, ${org}, ${modelP}, ${modelC}, ${rssi}, ${txPower}"
        )
    }
}
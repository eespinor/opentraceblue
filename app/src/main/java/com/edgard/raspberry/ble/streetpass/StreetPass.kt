package com.edgard.raspberry.ble.streetpass

import com.edgard.raspberry.ble.BuildConfig


const val ACTION_DEVICE_SCANNED = "${BuildConfig.APPLICATION_ID}.ACTION_DEVICE_SCANNED"
